#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ASUS_AI2202.mk

COMMON_LUNCH_CHOICES := \
    lineage_ASUS_AI2202-user \
    lineage_ASUS_AI2202-userdebug \
    lineage_ASUS_AI2202-eng
